<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Product;
use App\Models\Category;
use App\Models\Offer;
use App\Models\CategoryProduct;

class Parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client([
            'base_uri' => 'https://markethot.ru'
        ]);
        $response = $client->request('GET', '/export/bestsp');
        $data = json_decode($response->getBody(), true);
        $products = $data['products'];
        $this->insertAll($products);
    }
    public function insertAll($products) {
        $this->insertCategories($products);
        $this->insertProducts($products);
        $this->insertOffers($products);
    }

    public function sortCategories($categories, $parentId = null) {
        $sortedCategories = [];
    
        // find by parent
        foreach ($categories as $k => $category) {
            if($category['parent'] == $parentId) {
                // save parent
                $sortedCategories[] = $category;
                // find children
                $sortedCategories = array_merge($sortedCategories, $this->sortCategories($categories, $category['id']));
            }
        }
    
        return $sortedCategories;
    }

    public function insertCategories($products) {
        $categories = [];
        foreach($products as $product) {
            foreach($product['categories'] as $category) {
                if(!isset($categories[$category['id']])) {
                    $categories[$category['id']] = $category;
                }
            }
        }
        $allCategories = [];
        $tree = $this->sortCategories($categories);
        foreach($tree as $category) {
            $allCategories[] = [
                'id' => $category['id'],
                'title' => $category['title'],
                'alias' => $category['alias'],
                'parent' => $category['parent']
            ];
        }
        $res = Category::insert($allCategories); 

    }

    public function insertCategoryProduct($products) {
        foreach($products as $product) {
            $list = [];
            foreach($product['categories'] as $category) {
                $list[] = $category['id'];
            }
            
            Product::find($product['id'])->categories()->sync($list);
        }
    }

    public function insertProducts($products) {
        $listProducts = [];
        foreach($products as $product) {
            $listProducts[] = [
                'id' => $product['id'],
                'title' => $product['title'],
                'image' => $product['image'],
                'description' => $product['description'],
                'first_invoice' => $product['first_invoice'],
                'url' => $product['url'],
                'price' => $product['price'],
                'amount' => $product['amount']
            ];
        }
        $res = Product::insert($listProducts);
        $this->insertCategoryProduct($products);
    } 
    public function insertOffers($products) {
        $offers = [];
        foreach($products as $product) {
            foreach($product['offers'] as $offer) {
                $offer['product_id'] = $product['id']; 
                $offers[] = $offer;
               
            }
        }
        $res = Offer::insert($offers);
    } 
}


