<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Product;
use App\Models\Category;
use App\Models\Offer;
use App\Models\CategoryProduct;

class TestController extends Controller
{
    protected $sortCat = [];

    public function modelTest() {
        $offers = Product::find(1)->offers;
        dump($offers); // correct

        $product = Offer::find(1)->product;
        dump($product); // correct

        $categories = Product::find(1)->categories;
        dump($categories); // correct
        $products = Category::find(8)->products;
        dump($products); // correct
    }

    public function guzzle() {
        $client = new Client([
            'base_uri' => 'https://markethot.ru'
        ]);
        $response = $client->request('GET', '/export/bestsp');
        $data = json_decode($response->getBody(), true);
        $products = $data['products'];
      //  $this->insertAll($products);
      $this->insertCategoryProduct($products);
    }

    public function insertAll($products) {
        $this->insertCategories($products);
        $this->insertProducts($products);
        $this->insertOffers($products);
    }
    // function sortCategories($categories, $parentId = null) {
    //     $sortCategories = [];
    //     foreach($categories as $category) {
    //         if($category['parent'] === $parentId) {
    //             $sortCategories[] = $category;
    //         }
    //     }
    //     return array_merge($sortCategories, sortCategories($categories, $category['id']));
    // }
    public function sortCategories($categories, $parentId = null) {
        $sortedCategories = [];
    
        // find by parent
        foreach ($categories as $k => $category) {
            if($category['parent'] == $parentId) {
                // save parent
                $sortedCategories[] = $category;
                // find children
                $sortedCategories = array_merge($sortedCategories, $this->sortCategories($categories, $category['id']));
            }
        }
    
        return $sortedCategories;
    }

    public function insertCategories($products) {
        $categories = [];
        foreach($products as $product) {
            foreach($product['categories'] as $category) {
                if(!isset($categories[$category['id']])) {
                    $categories[$category['id']] = $category;
                }
            }
        }
        $allCategories = [];
        $tree = $this->sortCategories($categories);
        foreach($tree as $category) {
            $allCategories[] = [
                'id' => $category['id'],
                'title' => $category['title'],
                'alias' => $category['alias'],
                'parent' => $category['parent']
            ];
        }
        $res = Category::insert($allCategories); 

    }

    public function insertCategoryProduct($products) {
        foreach($products as $product) {
            $list = [];
            foreach($product['categories'] as $category) {
                $list[] = $category['id'];
            }
            
            Product::find($product['id'])->categories()->sync($list);
        }
    }

    public function insertProducts($products) {
        $listProducts = [];
        foreach($products as $product) {
            $listProducts[] = [
                'id' => $product['id'],
                'title' => $product['title'],
                'image' => $product['image'],
                'description' => $product['description'],
                'first_invoice' => $product['first_invoice'],
                'url' => $product['url'],
                'price' => $product['price'],
                'amount' => $product['amount']
            ];
        }
        $res = Product::insert($listProducts);
        $this->insertCategoryProduct($products);
    } 
    public function insertOffers($products) {
        $offers = [];
        foreach($products as $product) {
            foreach($product['offers'] as $offer) {
                $offer['product_id'] = $product['id']; 
                $offers[] = $offer;
               
            }
        }
        $res = Offer::insert($offers);
    } 

    //     public function childrenCreate($childrens) {
    //     foreach($childrens as $children) {
    //         $this->createCategory($children);
    //         $this->childrenCreate($children['children']);
    //     }
    //     return true;
    // }   
  
    // public function createCategory($category) {
    //     Category::create([
    //         'id' => $category['id'],
    //         'title' => $category['title'],
    //         'alias' => $category['alias'],
    //         'parent' => $category['parent']
    //     ]);
    // }
    // public function sort($tree) {
    //     $sort = [];
    //     foreach($tree as $category) {
    //         $arrs = $this->test($category);
    //         $sort = array_merge($sort, $arrs);
    //     }
    //     return $sort;
    // }

    // public function test($category) {
    //     $arr = [];
    //     if(!empty($category)) {
    //     $arr[] = $category;
    //     foreach($category['children'] as $children) {
    //         $arr[] = $children;
    //         $res = $this->test($children['children']);
    //         foreach($res as $array) { $arr[] = $array; }
    //     }
    //  }
    //     return $arr;
    // }

    // public function makeTree($categories, $parentId = null) {
    //     $parentCategories = [];
    //     foreach($categories as $category) {
    //         if($category['parent'] == $parentId) {
    //             $parentCategories[] = $category;
    //         }
    //     }

    //     foreach($parentCategories as $key => $parentCategory) {
    //         $parentCategories[$key]['children'] = $this->makeTree($categories, $parentCategory['id']);
    //     }

    //     return $parentCategories;
    // }

    
}
