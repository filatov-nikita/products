<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Category;

class MainController extends Controller
{
    public function getIndex() {
        $popularOffers = Offer::orderBy('sales', 'desc')->groupBy('product_id')->limit(20)->get();
        $categories = Category::where('parent' , null)->get();
        return view('primary', ['page' => 'pages.index', 'popularOffers' => $popularOffers, 'categories' => $categories]);    
    }

    public function getProduct($id) {
        $product = Product::findOrFail($id);
        return view('primary', ['page' => 'pages.product', 'product' => $product]);
    }
    public function getCategoryProduct($slug) {
        $category = Category::where('alias', $slug)->first();
        if(!$category) abort(404);
        $products = $category->products;
        return view('primary', ['page' => 'pages.list_products', 'products' => $products, 'category' => $category]);
    }
}
