<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function offers() {
        return $this->hasMany('App\Models\Offer');
    }
    
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }
}
