<div class="products">
    <div class="container">
        <h2>{{$category->title}}</h2>
        @foreach($products as $product)
            @include('parts.part_product')
        @endforeach
    </div>
</div>