<h1>Популярные товары</h1>
<div class="products">
    <div class="container">
        @foreach($popularOffers as $offer)
            <div><img src="{{$offer->product->image}}" alt=""></div>
            <div>
                <a href="{{route('product', ['id' => $offer->product->id])}}">{{$offer->product->title}}</a>
            </div>
            <div>Цена: {{$offer->product->price}} руб.</div>
        @endforeach
    </div>
</div>

<br>

<div class="categories">
    @foreach($categories as $category)
        <div class="link">
            <a href="{{route('category', ['slug' => $category->alias])}}">{{$category->title}}</a>
        </div>
    @endforeach
</div>