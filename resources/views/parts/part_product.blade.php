<div class="product">
        <div><a href="{{route('product', ['id' => $product->id])}}">{{$product->title}}</a></div>
        <div><img src="{{$product->image}}" alt=""></div>
        <div>
            <p>{{$product->description}}</p>
        </div>
</div>